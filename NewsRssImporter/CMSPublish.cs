﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Net.Http;


namespace NewsRssImporter
{

    public class Form
    {
        public string FormAction { get; set; }
        public string FormMethod { get; set; }
        public Dictionary<string, string> Fields { get; set; }

        public bool ParseString(string html)
        {
            try
            {
                //we can't use teh xml parsers because the html is malformed.
//                var xhtml = XDocument.Parse(html);
                var formHtml = GetGroup(html, "(<form.*</form>)");
                if (formHtml == null) return false;
                FormMethod = GetGroup(formHtml, "method=\"([^\"]*)\"");
                FormAction = GetGroup(formHtml, "action=\"([^\"]*)\"");
                Fields = GetMatches(formHtml, "<input.*?name=\"(?<key>[^\"]*)\".*?>","value=\"([^\"]*)\"");
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        protected string GetGroup(string input, string regexString)
        {
            var regex = new Regex(regexString,RegexOptions.IgnoreCase | RegexOptions.Singleline);
            var match = regex.Match(input);
            if (!match.Success) return null;
            if (match.Groups.Count < 2) return null;
            if (!match.Groups[1].Success) return null;
            return match.Groups[1].Value;
        }

        protected Dictionary<string, string> GetMatches(string input, string regexString,string valueString)
        {
            var regex = new Regex(regexString,RegexOptions.IgnoreCase | RegexOptions.Singleline);
            var regexValue = new Regex(valueString,RegexOptions.IgnoreCase | RegexOptions.Singleline);
            var matches = regex.Matches(input);
            var ret = new Dictionary<string, string>();
            foreach (Match match in matches)
            {
                if (match.Success & match.Groups["key"].Success)
                {
                    var key = match.Groups["key"].Value;
                    var valueMatch = regexValue.Match(match.Value);
                    var value = string.Empty;
                    if (valueMatch.Success)
                    {
                        value = valueMatch.Groups[1].Value;
                    }
                    ret.Add(key,value);
                }
            }
            return ret;
        }

    }

    static class CmsPublish
    {
        static CmsPublish()
        {
            cookies = new CookieContainer();
        }

        public static void Publish()
        {
            Login();
        }

        private static CookieContainer cookies;

        static void Login(string username = "newsreleasepublish", string password = "PUT YOUR PASSWORD HERE")
        {
            var CMSUrl = ConfigurationManager.AppSettings["CMSUrl"];

            var formString = HttpGet(CMSUrl+"login.aspx?AcceptsCookies=yes").Result;
            var formData = new Form();
            formData.ParseString(formString);
            var passwordField = formData.Fields.Keys.First(k => k.Contains("password"));
            var userField = formData.Fields.Keys.First(k => k.Contains("username"));
            formData.Fields[passwordField] = password;
            formData.Fields[userField] = username;
            var loginStatus = HttpPost(CMSUrl+"login.aspx?AcceptsCookies=yes", formData.Fields).Result;
            Console.WriteLine("logged in and about to publish");
            var publishText = HttpPost(CMSUrl+"system/priorityPublish.aspx?pid=40&id=244005").Result; //update cookies These urls are your news pages, you will need to customize it 
            publishText = HttpPost(CMSUrl+"system/prioritypublishlaunch.aspx?id=244005&pid=40").Result;
            var publishForm = new Form();
            publishForm.ParseString(publishText);
            publishText = HttpPost(CMSUrl+"system/prioritypublishlaunch.aspx?id=244005&pid=40",publishForm.Fields).Result;
            publishText = HttpPost(CMSUrl+"system/checkPriorityPublishState.aspx?ID=5&d=42").Result;
            var logoutString = HttpGet(CMSUrl + "system/logout.aspx").Result;
        }

        static async Task<string> HttpPost(string url, Dictionary<string, string> postData = null)
        {
            using (var handler = new HttpClientHandler
            {
                AllowAutoRedirect = false,
                UseCookies = true,
                CookieContainer = cookies
            })
            {
                using (var client = new HttpClient(handler))
                {
                    
                    if (postData == null)
                    {
                        postData = new Dictionary<string, string>();
                    }

                    var content = new FormUrlEncodedContent(postData);
                    client.DefaultRequestHeaders.Add("User-Agent","Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36");
                    var response = await client.PostAsync(url, content);
                    return response.Content.ReadAsStringAsync().Result;


                }
            }
        }


        static async Task<string> HttpGet(string url)
        {
            using (var handler = new HttpClientHandler
            {
                AllowAutoRedirect = true,
                UseCookies = true,
                CookieContainer = cookies
            })
            {
                using (var client = new HttpClient(handler))
                {
                     client.DefaultRequestHeaders.Add("User-Agent","Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36");
                    var responsestring = await client.GetStringAsync(url);
                    return responsestring;
                }
            }
        }

    }
}
