﻿using System.Net;
using ElementTree;
using log4net;
using log4net.Config;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Xml.Linq;
using NewsRssImporter;

namespace NewsItemImporter
{
    internal class Program
    {
        private readonly static ILog log;

        static Program()
        {
            Program.log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        }

        public Program()
        {
        }

        private static IEnumerable<Page> GetCmsNewsItems()
        {
            Program.log.Info("Grabbing news items from ElementCMS");
            Language language = new Language(Convert.ToInt32(ConfigurationManager.AppSettings["langId"]), null);
            Page page = new Page(Convert.ToInt32(ConfigurationManager.AppSettings["pageId"]), null);
            Node navigation = Tree.GetNavigation("/topnav/news/", page, language, null);
            foreach (Page page1 in navigation.GetChildren(NodeType.Page).Cast<Page>())
            {
                foreach (Page page2 in page1.GetChildren(NodeType.Page).Cast<Page>())
                {
                    yield return page2;
                }
            }
        }

        private static IEnumerable<Program.NewsItem> GetRssNewsItems()
        {
            Program.log.Info("Grabbing news items from the RSS feed");
            XNamespace item = ConfigurationManager.AppSettings["atomNamespace"];
            XNamespace xNamespace = ConfigurationManager.AppSettings["cnanNamespace"];
            XDocument xDocument = XDocument.Load(ConfigurationManager.AppSettings["url"]);
            foreach (XElement xElement in xDocument.Root.Elements(item + "entry"))
            {
                XElement xElement1 = xElement.Element(xNamespace + "whatsnew");
                if (xElement1 == null || !(xElement1.Value == "true"))
                {
                    continue;
                }
                Program.NewsItem newsItem = new Program.NewsItem()
                {
                    Date = Convert.ToDateTime(xElement.Element(item + "updated").Value),
                    Url = xElement.Element(item + "link").Attribute("href").Value,
                    Name = xElement.Element(item + "title").Value,
                    Content = xElement.Element(item + "content").Value
                };
                yield return newsItem;
            }
        }

        private static int Import()
        {
            Program.log.Info(string.Concat("Importing RSS news feed at ", DateTime.Now));
            List<Program.NewsItem> rssItems = Program.GetRssNewsItems().ToList<Program.NewsItem>();
            List<Page> cmsItems = Program.GetCmsNewsItems().ToList<Page>();

            var tmplist = cmsItems.Where(i => i.StartDate > new DateTime(2015, 6, 1));

            List<Program.NewsItem> newItems = rssItems.Except<Program.NewsItem>(
                from i in cmsItems
                select new Program.NewsItem()
                {
                    Date = i.StartDate,
                    Name = i.Name
                }, new Program.NewsItemComparer()).ToList<Program.NewsItem>();
            Dictionary<int, int> yearsParents = (
                from i in cmsItems
                group i by new { Year = i.StartDate.Year } into g
                select g.First<Page>()).ToDictionary<Page, int, int>((Page x) => x.StartDate.Year, (Page x) => x.Parent);
            Program.log.Info(string.Concat(newItems.Count, " new items to process from the RSS feed"));
            foreach (Program.NewsItem item in newItems)
            {
                Program.log.InfoFormat("{0}, {1}", item.Date, item.Name);
                if (yearsParents.ContainsKey(item.Date.Year))
                {
                    DateTime date = item.Date;
                    Page archive = new Page(yearsParents[date.Year], null);
                    Page page1 = new Page(null)
                    {
                        StartDate = item.Date,
                        EndDate = item.Date,
                        Name = item.Name,
                        IsRedirect = true,
                        Status = StatusType.Complete,
                        IsStatic = false,
                        IsHidden = false,
                        RedirectTarget = "",
                        IsActive = true,
                        IsCompleted = true
                    };
                    Page page = page1;
                    Item item1 = new Item(null)
                    {
                        Name = "redirect",
                        Sharing = ShareType.Normal,
                        Content = item.Url,
                        PendingContent = null
                    };
                    Item link = item1;
                    archive.Add(page);
                    page.Add(link);
                }
                else
                {
                    Program.log.WarnFormat("No archive found for year: {0}; skipping. Create the archive and add at least one news item to import news item for that year", item.Date.Year);
                }
            }
            Program.log.Info("Import complete");
            return newItems.Count;
        }

        public static void Main(string[] args)
        {
            BasicConfigurator.Configure();
            try
            {
                if (Program.Import() > 0)
                {
                    CmsPublish.Publish();
                }
            }
            catch (Exception exception)
            {
                Program.log.Fatal("There was a problem completing the import", exception);
            }
        }

        private class NewsItem
        {
            public string Content
            {
                get;
                set;
            }

            public DateTime Date
            {
                get;
                set;
            }

            public string Name
            {
                get;
                set;
            }

            public string Url
            {
                get;
                set;
            }

            public NewsItem()
            {
            }
        }

        private class NewsItemComparer : IEqualityComparer<Program.NewsItem>
        {
            public NewsItemComparer()
            {
            }

            public bool Equals(Program.NewsItem x, Program.NewsItem y)
            {
                if (x.Name != y.Name)
                {
                    return false;
                }
                return x.Date == y.Date;
            }

            public int GetHashCode(Program.NewsItem obj)
            {
                DateTime date = obj.Date;
                return date.GetHashCode() * 17 + obj.Name.GetHashCode();
            }
        }
    }
}